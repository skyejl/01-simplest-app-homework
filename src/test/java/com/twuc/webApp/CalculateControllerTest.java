package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest
@AutoConfigureMockMvc
class CalculateControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_get_plus_table_test() throws Exception {
        String expectResult = "1+1=2  \n" +
                "2+1=3  2+2=4  \n" +
                "3+1=4  3+2=5  3+3=6  \n" +
                "4+1=5  4+2=6  4+3=7  4+4=8  \n" +
                "5+1=6  5+2=7  5+3=8  5+4=9  5+5=10 \n" +
                "6+1=7  6+2=8  6+3=9  6+4=10 6+5=11 6+6=12 \n" +
                "7+1=8  7+2=9  7+3=10 7+4=11 7+5=12 7+6=13 7+7=14 \n" +
                "8+1=9  8+2=10 8+3=11 8+4=12 8+5=13 8+6=14 8+7=15 8+8=16 \n" +
                "9+1=10 9+2=11 9+3=12 9+4=13 9+5=14 9+6=15 9+7=16 9+8=17 9+9=18 ";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus").contentType(MediaType.TEXT_PLAIN))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.content().string(expectResult));
    }

    @Test
    void get_multiply_table_test() throws Exception {
        String expectResult = "1*1=1  \n" +
                "2*1=2  2*2=4  \n" +
                "3*1=3  3*2=6  3*3=9  \n" +
                "4*1=4  4*2=8  4*3=12 4*4=16 \n" +
                "5*1=5  5*2=10 5*3=15 5*4=20 5*5=25 \n" +
                "6*1=6  6*2=12 6*3=18 6*4=24 6*5=30 6*6=36 \n" +
                "7*1=7  7*2=14 7*3=21 7*4=28 7*5=35 7*6=42 7*7=49 \n" +
                "8*1=8  8*2=16 8*3=24 8*4=32 8*5=40 8*6=48 8*7=56 8*8=64 \n" +
                "9*1=9  9*2=18 9*3=27 9*4=36 9*5=45 9*6=54 9*7=63 9*8=72 9*9=81 ";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/multiply").contentType(MediaType.TEXT_PLAIN))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
                .andExpect(MockMvcResultMatchers.content().string(expectResult));
    }

    @Test
    void get_range_plus_table_test_pass() throws Exception {
        String expectResult = "3+1=4  3+2=5  3+3=6  \n" +
                "4+1=5  4+2=6  4+3=7  4+4=8  \n" +
                "5+1=6  5+2=7  5+3=8  5+4=9  5+5=10 ";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus").
                param("start", "3").
                param("end", "5").contentType(MediaType.TEXT_PLAIN))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(expectResult));
    }

    @Test
    void get_range_plus_table_test_left_bigger_right_error() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/plus").
                param("start", "9").
                param("end", "5").contentType(MediaType.TEXT_PLAIN))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void get_range_multiply_table_test() throws Exception {
        String expectResult = "3*1=3  3*2=6  3*3=9  \n" +
                "4*1=4  4*2=8  4*3=12 4*4=16 \n" +
                "5*1=5  5*2=10 5*3=15 5*4=20 5*5=25 ";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/multiply").
                param("start", "3").
                param("end", "5").contentType(MediaType.TEXT_PLAIN))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(expectResult));
    }

    @Test
    void check_input_result_test_pass() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new Calculator(3, 5, "+", 8))))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.correct").value(true));
    }

    @Test
    void check_input_result_test_error() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new Calculator(3, 5, "+", 7))))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.correct").value(false));
    }

    @Test
    void check_operand_argument_illegal_error() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new Calculator(-4, 5, "+", 8))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void check_operator_argument_illegal_error() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new Calculator(4, 5, "-", 8))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void use_variable_check_type() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new Calculator(4, 5, "+", 8, ">"))))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.correct").value(true));
    }
    @Test
    void use_illegal_check_type() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/check")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new Calculator(4, 5, "+", 8, "?"))))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_response_when_media_type_json() throws Exception {
        String expectResult = "[[\"4*1=4\",\"\",\"4*2=8\",\"\",\"4*3=12\",\"4*4=16\"],[\"5*1=5\",\"\",\"5*2=10\",\"5*3=15\",\"5*4=20\",\"5*5=25\"]]";
        mockMvc.perform(MockMvcRequestBuilders.get("/api/tables/multiply").
                param("start", "4").
                param("end", "5").contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string(expectResult));
    }
}
