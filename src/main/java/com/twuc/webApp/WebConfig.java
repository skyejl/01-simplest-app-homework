//package com.twuc.webApp;
//
//        import org.springframework.context.annotation.ComponentScan;
//        import org.springframework.context.annotation.Configuration;
//        import org.springframework.http.MediaType;
//        import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
//        import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//        import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//@EnableWebMvc
//@Configuration
//@ComponentScan({"com.twuc.webApp"})
//public class WebConfig implements WebMvcConfigurer {
//    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
//        configurer.favorPathExtension(true).
//                favorParameter(false).
//                parameterName("mediaType").
//                ignoreAcceptHeader(false).
//                defaultContentType(MediaType.APPLICATION_JSON).
//                mediaType("html", MediaType.TEXT_HTML).
//                mediaType("text", MediaType.TEXT_PLAIN).
//                mediaType("json", MediaType.APPLICATION_JSON);
//    }
//
//}
