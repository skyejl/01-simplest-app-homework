package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.regex.Pattern;

@RestController
public class CalculateController {
    @PostMapping("/api/check")
    public ResponseEntity checkInput(@RequestBody Calculator calculator) {
        Integer operandLeft = calculator.getOperandLeft();
        Integer operandRight = calculator.getOperandRight();
        String operation = calculator.getOperation();
        String checkType = calculator.getCheckType();
        Integer expectedResult = calculator.getExpectedResult();
        Pattern pattern = Pattern.compile("[0-9]*");
        Boolean left = pattern.matcher(operandLeft.toString()).matches();
        Boolean right = pattern.matcher(operandRight.toString()).matches();
        Boolean result = pattern.matcher(operandLeft.toString()).matches();
        if (!left || !right || !(operation.equals("+") || operation.equals("*"))
                || !(checkType.equals("=") || checkType.equals("<") || checkType.equals(">"))) {
            throw new IllegalArgumentException("argument invalid");
        }
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON)
                .body(new Check(calculator.check()));
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity handleArgumentError(Exception exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(exception.getMessage());
    }

    @GetMapping(value = "/api/tables/multiply")
    public @ResponseBody
    ResponseEntity getMultiplyTable(@RequestHeader("Content-type") String contentType,
                                    @RequestParam(defaultValue = "1", name = "start") Integer operandLeft,
                                    @RequestParam(defaultValue = "9", name = "end") Integer operandRight) {
        if (operandLeft > operandRight) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        String text = new Calculator(operandLeft, operandRight, "*").mathCalculate();
        String[] arr = {};
        arr = text.split("\n");
        ArrayList<Object> list = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            list.add(arr[i].split(" "));
        }
        if (contentType.equals("application/json;charset=UTF-8")) {
            return ResponseEntity.ok().body(list);
        }
        if (contentType.equals("text/html;charset=UTF-8")) {
        }
        return ResponseEntity.ok().body(new Calculator(operandLeft, operandRight, "*").mathCalculate());
    }

    @GetMapping(value = "/api/tables/plus")
    public @ResponseBody
    ResponseEntity getPlusTable(@RequestHeader("Content-type") String contentType,
                                @RequestParam(defaultValue = "1", name = "start") Integer operandLeft,
                                @RequestParam(defaultValue = "9", name = "end") Integer operandRight) {
        if (operandLeft > operandRight) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        String text = new Calculator(operandLeft, operandRight, "+").mathCalculate();
        String[] arr = {};
        arr = text.split("\n");
        ArrayList<Object> list = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            list.add(arr[i].split(" "));
        }
        if (contentType.equals("application/json;charset=UTF-8")) {
            return ResponseEntity.ok().body(list);
        }
        if (contentType.equals("text/html;charset=UTF-8")) {
        }
        return ResponseEntity.ok().body(new Calculator(operandLeft, operandRight, "+").mathCalculate());
    }


}
