package com.twuc.webApp;

import java.util.ArrayList;
import java.util.List;

public class Calculator {
    private Integer operandLeft;
    private Integer operandRight;
    private String operation;
    private Integer expectedResult;
    private String checkType = "=";

    public Calculator(Integer operandLeft, Integer operandRight, String operation, Integer expectedResult, String checkType) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
        this.checkType = checkType;
    }

    public Calculator(Integer operandLeft, Integer operandRight, String operation, Integer expectedResult) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
    }

    public Calculator() {
    }

    public Calculator(Integer operandLeft, Integer operandRight, String operation) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
    }

    public String getCheckType() {
        return checkType;
    }

    public Integer getOperandLeft() {
        return operandLeft;
    }

    public Integer getOperandRight() {
        return operandRight;
    }

    public String getOperation() {
        return operation;
    }

    public Integer getExpectedResult() {
        return expectedResult;
    }

    public String mathCalculate() {
        List<String> list = new ArrayList<>();
        StringBuffer plusline = new StringBuffer();
        StringBuffer multline = new StringBuffer();
        Integer maxPlusLength = 0;
        Integer maxMultipleLength = 0;
        maxPlusLength = plusline.append(this.operandRight).append("+").append(this.operandRight).append("=").append(this.operandRight+this.operandRight).toString().length() + 1;
        maxMultipleLength = multline.append(this.operandRight).append("+").append(this.operandRight).append("=").append(this.operandRight*this.operandRight).toString().length() + 1;
        for(int i = this.operandLeft; i <= this.operandRight; i++) {
            String line = "";
            for(int j = 1; j <= i; j++) {
                if (this.operation == "+") {
                    line += String.format("%-"+maxPlusLength+"s", i+("+")+j+this.checkType+(i+j));
                }
                if (this.operation == "*" ) {
                    line += String.format("%-"+maxMultipleLength+"s", i+("*")+j+this.checkType+(i*j));
                }
            }
            list.add(line);
        }
        return String.join("\n", list);
    }

    public boolean check() {
        String type = this.checkType;
        switch (type) {
            case "=":
                if(this.operation.equals("+")) {
                    return expectedResult == this.operandLeft + this.operandRight;
                }
                if(this.operation.equals("*")){
                    return expectedResult == this.operandLeft * this.operandRight;
                }
                break;
            case ">":
                if(this.operation.equals("+")) {
                    return this.operandLeft + this.operandRight > expectedResult;
                }
                if(this.operation.equals("*")){
                    return  this.operandLeft * this.operandRight > expectedResult;
                }
                break;
            case "<":
                if(this.operation.equals("+")) {
                    return this.operandLeft + this.operandRight < expectedResult;
                }
                if(this.operation.equals("*")){
                    return this.operandLeft * this.operandRight < expectedResult;
                }
                break;
        }
        return false;
    }
}
