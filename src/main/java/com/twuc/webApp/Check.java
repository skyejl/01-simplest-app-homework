package com.twuc.webApp;

public class Check {
    private Boolean correct;

    public Boolean getCorrect() {
        return correct;
    }

    public Check(Boolean correct) {
        this.correct = correct;
    }
}
